SELECT format('ALTER TABLE plans.%I SET SCHEMA public;', relname) FROM pg_class  WHERE relkind = 'r' AND relnamespace = 'plans'::regnamespace \gexec
SELECT format('ALTER TABLE public.%I rename to %I;', relname, 'plans_' || relname) FROM pg_class WHERE relkind = 'r' AND relnamespace = 'public'::regnamespace AND relname ~ '^part_[0-9]+$' \gexec
DROP SCHEMA plans;
